require('babel-register')
const { success, error } = require('./node_modules/functions/functions')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const express = require('express')
const morgan = require('morgan')
const config = require('./assets/config')

const db = mysql.createConnection({
    host: config.db.host,
    database: config.db.database,
    user: config.db.user,
    password: config.db.password
})
//Test modif git
db.connect((err) => {

    if (err)
        console.log(err.message);
    else {
        console.log('Connected');

        const app = express()

        let MembersRouter = express.Router()

        app.use(morgan('dev'))
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));

        MembersRouter.route('/:id')

            // Récupère un membre avec son ID
            .get((req, res) => {

                db.query('SELECT * FROM members WHERE id = ?', [req.params.id],(err,result) => {
                    if(err) {
                        res.json(error(err.message))
                    } else {
                        
                        if (result[0] != undefined){
                            res.json(success(result))
                        } else {
                            res.json(error(error('Wrong id value')))
                        }
                    }
                })
            })

            // Modifie un membre avec ID
            .put((req, res) => {

                let index = getIndex(req.params.id);

                if (typeof (index) == 'string') {
                    res.json(error(index))
                } else {

                    let same = false

                    for (let i = 0; i < members.length; i++) {
                        if (req.body.name == members[i].name && req.params.id != members[i].id) {
                            same = true
                            break
                        }
                    }

                    if (same) {
                        res.json(error('same name'))
                    } else {
                        members[index].name = req.body.name
                        res.json(success(true))
                    }

                }

            })

            // Supprime un membre avec ID
            .delete((req, res) => {

                let index = getIndex(req.params.id);

                if (typeof (index) == 'string') {
                    res.json(error(index))
                } else {

                    members.splice(index, 1)
                    res.json(success(members))

                }

            })

        MembersRouter.route('/')

            // Récupère tous les membres
            .get((req, res) => {
                if (req.query.max != undefined && req.query.max > 0) {

                    db.query('SELECT * FROM members LIMIT 0, ?', [req.query.max], (err, result) => {
                        if (err) {
                            res.json(error(err.message))
                        } else {
                            res.json(success(result))
                        }
                    })

                } else if (req.query.max != undefined) {
                    res.json(error('Wrong max value'))
                } else {

                    db.query('SELECT * FROM members', (err, result) => {
                        if (err) {
                            res.json(error(err.message))
                        } else {
                            res.json(success(result))
                        }
                    })
                }
            })

            // Ajoute un membre avec son nom
            .post((req, res) => {

                if (req.body.name) {

                    let sameName = false
                    for (let i = 0; i < members.length; i++) {
                        if (members[i].name == req.body.name) {
                            sameName = true
                            break
                        }
                    }

                    if (sameName) {
                        res.json(error('name already taken'))
                    } else {
                        let member = {
                            id: createID(),
                            name: req.body.name
                        }
                        members.push(member)
                        res.json(success(member))
                    }

                } else {
                    res.json(error('no name value'))
                }

            })

        app.use(config.rootAPI + 'members', MembersRouter)

        app.listen(config.port, () => console.log('Started on port ' + config.port))
    }
})



function getIndex(id) {
    for (let i = 0; i < members.length; i++) {
        if (members[i].id == id)
            return i
    }
    return 'wrong id'
}

function createID() {
    return members[members.length - 1].id + 1
}